# object-to-string-path-array

This little library converts an object to an array of (string) paths.  

For example:
```
import toPathArray from 'object-to-string-path-array';
toPathArray({
    aaa: 'bbb',
    ccc: ['ddd', 'eee'],
    fff: {
        ggg: {
            hhh: ['iii', 'jjj']
        }
    }
}, '/');
```
Will return:
```
[
    'aaa/bbb',
    'ccc/ddd',
    'ccc/eee',
    'fff/ggg/hhh/iii',
    'fff/ggg/hhh/jjj'
]
```