declare function convert(input: {
    [x: string]: any;
} | string[] | string, concatenator?: string): string[];
export default convert;
