"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function convert(input, concatenator) {
    if (concatenator === void 0) { concatenator = '/'; }
    if (typeof input === 'string') {
        return [input];
    }
    else if (input instanceof Array) {
        return input;
    }
    else if (typeof input === 'object') {
        var values = Object.keys(input).map(function (key) {
            var convertedValue = Object.keys(input).indexOf(key) >= 0
                ? convert(input[key])
                : [];
            return convertedValue.length > 0
                ? convertedValue.map(function (x) { return "" + key + concatenator + x; })
                : [key];
        });
        return values.reduce(function (result, next) {
            return result.concat(next);
        }, []);
    }
    else {
        return [];
    }
}
exports.default = convert;
