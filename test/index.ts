import f from '../src';
import { expect } from 'chai';

describe('Function with string object', () => {
	it('should convert an empty object to an empty array' , () => {
		expect(f({})).to.deep.equal([]);
	});

	it('should convert object with string values' , () => {
		expect(f({
			foo: 'bar',
			baz: 'quux'
		})).to.deep.equal([
			'foo/bar',
			'baz/quux'
		]);
	});

	it('should convert object with nested array values' , () => {
		expect(f({
			foo: ['bar', 'baz']
		})).to.deep.equal([
			'foo/bar',
			'foo/baz'
		]);
	});

	it('should convert object with nested object values' , () => {
		expect(f({
				foo: {
					bar: 'baz',
					quux: 'quuz'
				}
		})).to.deep.equal([
			'foo/bar/baz',
			'foo/quux/quuz'
		]);
	});

	it('should convert the README example correctly' , () => {
		expect(f({
			aaa: 'bbb',
			ccc: ['ddd', 'eee'],
			fff: {
					ggg: {
							hhh: ['iii', 'jjj']
					}
			}
		})).to.deep.equal([
			'aaa/bbb',
			'ccc/ddd',
			'ccc/eee',
			'fff/ggg/hhh/iii',
			'fff/ggg/hhh/jjj'
		]);
	});

	it('should use a key without values' , () => {
		expect(f({
			aaa: [],
			bbb: ['ccc', 'ddd']
		})).to.deep.equal([
			'aaa',
			'bbb/ccc',
			'bbb/ddd'
		]);
	});
});