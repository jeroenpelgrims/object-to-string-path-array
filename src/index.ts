function convert(
	input: {[x:string]: any} | string[] | string,
	concatenator='/'
): string[] {
	if (typeof input === 'string') {
		return [ input ];
	} else if (input instanceof Array) {
		return input;
	} else if (typeof input === 'object') {
		const values = Object.keys(input).map(key => {
			const convertedValue = convert(input[key] as {[x:string]: any})
			
			return convertedValue.length > 0
				? convertedValue.map(x => `${key}${concatenator}${x}`)
				: [key];
		});
		return values.reduce((result, next) => {
			return [...result, ...next];
		}, []);
	} else {
		return [];
	}
}

export default convert;